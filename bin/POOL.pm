# Copyright (c) 1998,1999,2001,2002,2003 Kevin Cameron
# Distributed under the GNU Lesser General Public License
# RCS ID: $Id: POOL.pm,v 1.20 2010/05/25 17:48:25 v-ms Exp $
 


$cls{NLD} = "Nailed";
$cls{DSK} = "OnDisk";
$cls{CTG} = "Contig";
$cls{VRT} = "Virtual";
$cls{MPD} = "Mapped";
$cls{INM} = "InMem";

return 1;
