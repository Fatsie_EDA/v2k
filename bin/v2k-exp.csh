#!/bin/csh -fx
# Copyright (c) 1998,1999,2001,2002,2003 Kevin Cameron
# Distributed under the GNU Lesser General Public License
# RCS ID: $Id: v2k-exp.csh,v 1.20 2009/05/27 18:37:39 v-ms Exp $
 
# construct distribution from CVS

umask 002

setenv REPOSITORY /software/source

set root = $cwd

set unm = (`uname -a`)
set apa = ()

switch ($unm[1])
  case SunOS:
    set tar = gtar
    breaksw
  default:
    set tar = tar
    set apa = (USR_NOTERMCAP=1 -shrd)
endsw

set exe=$0
set rlx=()
set slv=1
retry:
switch ($exe)
  case */*:
    set exe=$exe:t
    goto retry
  case spc*:
    set app=spice
    set vrsns="{top,3??}"
    breaksw
  default:
    set slv=0
    set app=v2k
    set apa=($apa:q -try 2 -shrd)
    set vrsns="{top,??.??}"
endsw

switch ($1)
  case +x:
    shift
    cd $1
    shift
    goto fix
  case +b:
    shift
    cd $1
    shift
    goto bld
endsw

(rm -f ${app}-${vrsns})
mkdir junk-$$
(eval mv ${app}-${vrsns} junk-$$)
(nice rm -fr junk-$$ &)

mkdir ${app}-top
cvs export -r HEAD -d ${app}-top ${app}

cd ${app}-top

fix:
if $slv then
  if ( ! -d ../v2k-top ) then
    echo "Can't find v2k-top"
    exit 1
  else
    rm -f config bin
    ln -s ../v2k-top/config .
    ln -s ../v2k-top/bin    .
  endif
else
  ln -s mk.csh bin/mk
endif

set path = ($path:q `pwd`/bin)

mk bin_links src_tgz $rlx:q

mv export/*.gz ..

switch ($1)
  case -s:
    exit 1
endsw

cd $root
rm -fr ${app}-top

set rls = (`ls -t ${app}-*.tar.gz`)

set rls_tar = $rls[1]
set rls_top = $rls_tar:r
set rls_top = $rls_top:r

mv $rls_top $rls_top-$$
(nice rm -fr $rls_top-$$ &)

$tar -xvzf $rls_tar

set ${app}_top = $rls_tar:r
set ${app}_top = $rls_top:r

cd $rls_top

bld:
set path = ($path $cwd/bin)

mk setup
mk apps $apa
if ($status) exit $status
mk exports
mk docs -i

examples/test.csh
