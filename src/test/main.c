/* Copyright (c) 1998-2007 Kevin Cameron */
/* Distributed under the GNU Lesser General Public License */
/* RCS ID: */ const char * main_c_rcsid() {return "$Id: main.c,v 1.18 2010/05/25 17:48:28 v-ms Exp $";}
 



#include "system.h"
#include "args.h"

int main(int argc,char **argv)
{ 
  InitLang();
  return processArgs(&argc,&argv,0);
}
