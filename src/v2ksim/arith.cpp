/* Copyright (c) 1998-2007 Kevin Cameron */
/* Distributed under the GNU Lesser General Public License */
/* RCS ID: */ extern "C" const char * arith_cpp_rcsid() {return "$Id: arith.cpp,v 1.14 2010/05/25 17:48:28 v-ms Exp $";}
 
#include "system.h"

#include "sim_extra.h"
#include "simulation.h"

#include "fenum.h"
#include "strfunc.h"
#include "file.h"
#include "dyn.h"
#include "sim_kern.h"

inline void v2kSim::Eval(int si,int drv_n,void *drv_v)
{
  v2kSimEval(ip,si,drv_n,drv_v);
}

#define INLINE_ARITH
#include "sim_arith.inc"
