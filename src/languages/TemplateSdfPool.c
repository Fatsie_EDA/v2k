/* Copyright (c) 1998-2007 Kevin Cameron */
/* Distributed under the GNU Lesser General Public License */
/* RCS ID: */ const char * TemplateSdfPool_c_rcsid() {return "$Id: TemplateSdfPool.c,v 1.20 2010/05/25 17:48:27 v-ms Exp $";}
 


#undef  IREF
#define IREF(t)          t
#undef  VAR_ARRAY_PTR
#define VAR_ARRAY_PTR(n) *n
#define plCellInst       CellInst

#include "TemplateSdfPool.fld"
