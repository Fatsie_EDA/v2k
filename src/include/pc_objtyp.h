/* Copyright (c) 1998-2007 Kevin Cameron */
/* Distributed under the GNU Lesser General Public License */

#undef  pc_objtyp_h_rcsid
#define pc_objtyp_h_rcsid() {return "$Id: pc_objtyp.h,v 1.6 2007/10/02 17:54:08 v-ms Exp $";} /* RCS ID */

PC_OT_DECL(PCO_Null,      "",   "null",      FT_Null)
PC_OT_DECL(PCO_Class,     "cls","class",     FT_Null)

#undef PC_OT_DECL
