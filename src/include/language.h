/* Copyright (c) 1998-2007 Kevin Cameron */
/* Distributed under the GNU Lesser General Public License */
#undef  language_h_rcsid
#define language_h_rcsid() {return "$Id: language.h,v 1.13 2010/05/25 17:48:27 v-ms Exp $";} /* RCS ID */

#ifndef LANGUAGE_H
#define LANGUAGE_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

extern const char *LangName[];

extern void  InitLang(const char *,char *);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* LANGUAGE_H */
