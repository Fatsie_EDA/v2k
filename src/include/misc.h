/* Copyright (c) 1998,1999,2001,2002,2003 Kevin Cameron */
/* Distributed under the GNU Lesser General Public License */
#undef  misc_h_rcsid
#define misc_h_rcsid() {return "$id";} /* RCS ID */

 
#ifndef MISC_H
#define MISC_H

#ifdef __cplusplus
extern "C" {
#endif

  void CopyVirtual(const void *,void *);
  int  getRlimit(int,I64 *);
  int  setRlimit(int,I64);

#ifdef __cplusplus
}
#endif

typedef struct {
  int   lim,
        byt;
  char *name;
} Limit;

extern const Limit Limits[];

#ifndef UI_TYPES
typedef unsigned int   U32;
typedef int            I32;
typedef unsigned short U16;
typedef short          I16;
#endif

#endif
