/* Copyright (c) 1998,1999,2001,2002,2003 Kevin Cameron */
/* Distributed under the GNU Lesser General Public License */
#undef  ll_h_rcsid
#define ll_h_rcsid() {return "$id";} /* RCS ID */

 
#ifdef HAVE_LL

# define I64 long long
# define U64 unsigned long long

#define SET_LL(ll,vl) (ll = (vl))
#define LL2INT(ll)    (ll)

#else

/* Long-long type for non-supporting compilers */

# ifdef __cplusplus

class U64 {
 unsigned int hi,
              lo;
};

class I64 {
 int          hi;
 unsigned int lo;
};

#define SET_LL(ll,vl) (ll.hi = (vl) >> 32,ll.lo = (vl) & 0xFFFFFFFF)
#define LL2INT(ll)    (ll.lo)

# endif
#endif

