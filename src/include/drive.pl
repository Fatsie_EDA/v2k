# Copyright (c) 1998,1999,2001,2002,2003 Kevin Cameron
# Distributed under the GNU Lesser General Public License
# RCS ID: $Id: drive.pl,v 1.20 2010/05/25 17:48:27 v-ms Exp $
 


@lvls = ("0","1");
@strn = ("HIGHZ","SMALL","MEDIUM","WEAK","LARGE","PULL","STRONG","SUPPLY");
@cert = ("","U","X");
$drv  = "DRV";
