/* Copyright (c) 1998-2007 Kevin Cameron */
/* Distributed under the GNU Lesser General Public License */
#undef  template_h_rcsid
#define template_h_rcsid() {return "$Id: template.h,v 1.11 2010/05/25 17:48:27 v-ms Exp $";} /* RCS ID */

 
#ifndef POOL_H
#include "pool.h"
#endif
#ifndef POOLMNGR_H
#include "poolmngr.h"
#endif
#include "ref-pp.h"
#include "template-pp.h"
