/* Copyright (c) 1998-2007 Kevin Cameron */
/* Distributed under the GNU Lesser General Public License */
#undef  cdump_h_rcsid
#define cdump_h_rcsid() {return "$Id: cdump.h,v 1.11 2010/05/25 17:48:27 v-ms Exp $";} /* RCS ID */

 
#ifdef __cplusplus
extern "C" {
#endif

eSTS cDumpC (int *argc,const char ***argv,void *var);

#ifdef __cplusplus
extern "C" }
#endif
