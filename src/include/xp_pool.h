/* Copyright (c) 1998-2007 Kevin Cameron */
/* Distributed under the GNU Lesser General Public License */
#undef  xp_pool_h_rcsid
#define xp_pool_h_rcsid() {return "$Id: xp_pool.h,v 1.11 2010/05/25 17:48:27 v-ms Exp $";} /* RCS ID */

 
#include "template.h"
#include "ref-pp.h"
#include "xp_pool-pp.h"

#include "poolmngr.h"
