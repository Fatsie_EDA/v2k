/* Copyright (c) 1998-2007 Kevin Cameron */
/* Distributed under the GNU Lesser General Public License */
#undef  str_pool_h_rcsid
#define str_pool_h_rcsid() {return "$Id: str_pool.h,v 1.11 2010/05/25 17:48:27 v-ms Exp $";} /* RCS ID */

 
#include "template.h"
#include "ref-pp.h"
#include "str_pool-pp.h"

#include "poolmngr.h"

#define TOK_POOL_START 0x100

BasePool *CoerceStringPool(PoolIdentifier);
