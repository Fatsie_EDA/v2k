/* Copyright (c) 1998-2007 Kevin Cameron */
/* Distributed under the GNU Lesser General Public License */

#undef  pc_o_typ_h_rcsid
#define pc_o_typ_h_rcsid() {return "$Id: pc_o_typ.h,v 1.2 2010/05/25 17:48:27 v-ms Exp $";} /* RCS ID */

PC_OT_DECL(PCO_Null,      "",   "null",      FT_Null)
PC_OT_DECL(PCO_Class,     "cls","class",     FT_Null)

#undef PC_OT_DECL
