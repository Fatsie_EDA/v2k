/* Copyright (c) 1998-2007 Kevin Cameron */
/* Distributed under the GNU Lesser General Public License */
#undef  iref_h_rcsid
#define iref_h_rcsid() {return "$Id: iref.h,v 1.11 2010/05/25 17:48:27 v-ms Exp $";} /* RCS ID */

 
DECL_DEREF(void);
DECL_DEREF(char);
DECL_DEREF(long);
