/* Copyright (c) 1998-2007 Kevin Cameron */
/* Distributed under the GNU Lesser General Public License */
#undef  mod_pool_h_rcsid
#define mod_pool_h_rcsid() {return "$Id: mod_pool.h,v 1.11 2010/05/25 17:48:27 v-ms Exp $";} /* RCS ID */
 
#include "po_pool.h"
#include "mod_pool-pp.h"

BasePool        *CoerceModulePool(PoolIdentifier);
InMemModulePool *ModPool(int);

