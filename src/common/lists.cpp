/* Copyright (c) 1998-2007 Kevin Cameron */
/* Distributed under the GNU Lesser General Public License */
/* RCS ID: */ extern "C" const char * lists_cpp_rcsid() {return "$Id: lists.cpp,v 1.19 2010/05/25 17:48:26 v-ms Exp $";}

#include "v2k_mem.h"
#include "list.h"
#include "assert.h"

template class sortedList<void,true>;
template class sortedList<void,false>;
