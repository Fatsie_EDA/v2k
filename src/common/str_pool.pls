# Copyright (c) 1998,1999,2001,2002,2003 Kevin Cameron
# Distributed under the GNU Lesser General Public License
# RCS ID: $Id: str_pool.pls,v 1.19 2010/05/25 17:48:26 v-ms Exp $
#


STRUCT plStr
  short l;
  char  VAR_ARRAY(buff);
