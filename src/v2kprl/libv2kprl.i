/* Copyright (c) 1998,1999,2001,2002,2003 Kevin Cameron */
/* Distributed under the GNU Lesser General Public License */
/* RCS ID: */ const char * libv2kprl_i_rcsid() {return "$Id: libv2kprl.i,v 1.16 2010/05/25 17:48:28 v-ms Exp $";}
 


%module libv2kprl

extern int PerlV2Kinit(const char *exe);
extern int PerlV2Kdo  (int e,int s,const char *command);


